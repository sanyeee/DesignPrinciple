package com.sanyeee.designprinciple.lsp;

public abstract class AbstractGun {

    /**
     * 射击
     */
    public abstract void shoot();
}
