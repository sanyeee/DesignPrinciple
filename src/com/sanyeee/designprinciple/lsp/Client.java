package com.sanyeee.designprinciple.lsp;

public class Client {

    public static void main(String[] args) {
        AbstractGun gun = new HandGun();
        Soldier soldier = new Soldier();

        soldier.setGun(gun);
        soldier.killEnemy();
    }
}
