package com.sanyeee.designprinciple.dip;

/**
 * @author lxw
 */
public class Benz implements ICar {

    /**
     * 奔驰跑
     */
    @Override
    public void run()
    {
       System.out.println("Benz run");
    }
}
