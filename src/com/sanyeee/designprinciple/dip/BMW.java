package com.sanyeee.designprinciple.dip;

/**
 * 宝马
 */
public class BMW implements ICar {

    @Override
    public void run() {
        System.out.println("BMW run");
    }
}
