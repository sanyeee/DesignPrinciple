package com.sanyeee.designprinciple.dip;

/**
 * @author lxw
 */
public interface ICar {

    void run();
}
