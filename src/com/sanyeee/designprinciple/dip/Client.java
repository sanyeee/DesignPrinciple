package com.sanyeee.designprinciple.dip;

public class Client {

    public static void main(String[] args) {
        IDriver driver = new Dirver();
        ICar benz = new Benz();

        driver.drive(benz);
    }
}
