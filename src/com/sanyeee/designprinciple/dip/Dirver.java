package com.sanyeee.designprinciple.dip;

/**
 * @author lxw
 */
public class Dirver implements IDriver {

    @Override
    public void drive(ICar car) {
        car.run();
    }
}
