package com.sanyeee.designprinciple.dip;

/**
 * @author lxw
 */
public interface IDriver {

    /**
     * 开车
     * @param car
     */
    void drive(ICar car);
}
