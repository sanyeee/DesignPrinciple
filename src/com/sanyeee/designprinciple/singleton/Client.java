package com.sanyeee.designprinciple.singleton;

public class Client {
    public static void main(String[] args)
    {
       Writer.getInstance().write("This is the singleton principle.");
       Reader.getInstance().read();
    }
}
