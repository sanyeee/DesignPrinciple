package com.sanyeee.designprinciple.singleton;

/**
 * reader
 * @author lxw
 */
public class Reader {

    private static Reader reader = null;

    private Reader() {
    }

    /**
     * get single instance
     * @return
     */
    public static Reader getInstance() {
        if (null == reader) {
            synchronized (Reader.class) {
                if (null == reader) {
                    return new Reader();
                }
            }
        }

        return reader;
    }

    /**
     * read
     */
    public void read()
    {
        System.out.println("This is the safe singleton.");
    }
}
