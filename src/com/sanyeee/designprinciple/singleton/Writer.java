package com.sanyeee.designprinciple.singleton;

/**
 * Writer
 *
 * @author lxw
 */
public class Writer {
    private static final Writer writer = new Writer();

    /**
     * 私有构造函数
     */
    private Writer() {
    }

    /**
     * get the single instance
     *
     * @return
     */
    public static Writer getInstance() {
        if (null == writer) {
            return new Writer();
        }

        return writer;
    }

    /**
     * write
     *
     * @param context context
     */
    public void write(String context) {
        System.out.println(context);
    }

}
