package com.sanyeee.designprinciple.factory;

/**
 * 人类工程
 * @author lxw
 */
public class HumanFactory extends AbstractHumanFactory {

    @Override
    public <T extends IHuman> T createHuman(Class<T> clazz) {

        IHuman human = null;

        try {
            human = (IHuman) Class.forName(clazz.getName()).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return (T) human;
    }
}
