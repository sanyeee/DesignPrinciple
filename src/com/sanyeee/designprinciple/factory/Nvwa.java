package com.sanyeee.designprinciple.factory;

/**
 * 女娲类
 * @author lxw
 */
public class Nvwa {

    public static void main(String[] args) {
        AbstractHumanFactory humanFactory = new HumanFactory();

        IHuman human = humanFactory.createHuman(YellowHuman.class);
        human.talk();
        human.run();

        human = humanFactory.createHuman(WhiteHuman.class);
        human.talk();
        human.run();

        human = humanFactory.createHuman(BlackHuman.class);
        human.talk();
        human.run();
    }
}
