package com.sanyeee.designprinciple.factory;

/**
 * 抽象工程
 *
 * @author lxw
 */
public abstract class AbstractHumanFactory {

    /**
     * 创建人类
     * @param clazz
     * @param <T>
     * @return
     */
    public abstract <T extends IHuman> T createHuman(Class<T> clazz);
}
