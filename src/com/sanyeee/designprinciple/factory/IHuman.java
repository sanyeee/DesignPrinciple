package com.sanyeee.designprinciple.factory;

/**
 * 人类接口
 * @author lxw
 */
public interface IHuman {

    /**
     * 讲话
     */
    void talk();

    /**
     * 奔跑
     */
    void run();
}
