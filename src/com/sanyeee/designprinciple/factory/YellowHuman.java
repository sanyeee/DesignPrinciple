package com.sanyeee.designprinciple.factory;

/**
 * 黄种人
 */
public class YellowHuman implements IHuman {
    @Override
    public void talk() {
        System.out.println("yellow human talk");
    }

    @Override
    public void run() {
        System.out.println("yellow human run");
    }
}
