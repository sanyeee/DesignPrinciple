package com.sanyeee.designprinciple.factory;

/**
 * 黑种人
 */
public class BlackHuman implements IHuman {

    @Override
    public void talk() {
        System.out.println("black human talk");
    }

    @Override
    public void run() {
        System.out.println("black human run");
    }
}
