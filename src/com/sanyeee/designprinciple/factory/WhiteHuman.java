package com.sanyeee.designprinciple.factory;

/**
 * 白人
 */
public class WhiteHuman implements IHuman {

    @Override
    public void talk() {
        System.out.println("white human talk");
    }

    @Override
    public void run() {
        System.out.println("white human run");
    }
}
