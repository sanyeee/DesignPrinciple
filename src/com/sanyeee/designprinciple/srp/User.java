package com.sanyeee.designprinciple.srp;

/**
 * 用户
 */
public class User implements IUserDO, IUserBiz {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 电话号
     */
    private String phoneNum;


    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public void setUserName(String userName) {

        this.userName = userName;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public void setPassword(String password) {

    }

    @Override
    public String getPhoneNum() {
        return null;
    }

    @Override
    public void setPhoneNum(String phoneNum) {

    }

    @Override
    public boolean changeUserPassword(String password) {
        return false;
    }

    @Override
    public boolean changeUserPhoneNum(String phoneNum) {
        return false;
    }

    @Override
    public boolean cleanUserInfo() {
        return false;
    }
}
