package com.sanyeee.designprinciple.srp;

/**
 * 用户业务
 */
public interface IUserBiz {

    /**
     * 修改用户密码
     * @param password
     * @return
     */
    boolean changeUserPassword(String password);

    /**
     * 修改用户密码
     * @param phoneNum
     * @return
     */
    boolean changeUserPhoneNum(String phoneNum);

    /**
     * 清除用户信息
     * @return
     */
    boolean cleanUserInfo();
}
