package com.sanyeee.designprinciple.srp;

/**
 * 用户信息接口
 */
public interface IUserDO {

    /**
     * 获取用户名
     * @return
     */
    String getUserName();

    /**
     * 设置用户名
     * @param userName
     */
    void setUserName(String userName);

    /**
     * 获取密码
     * @return
     */
    String getPassword();

    /**
     * 设置密码
     * @param password
     */
    void setPassword(String password);

    /**
     * 获取手机号
     * @return
     */
    String getPhoneNum();

    /**
     * 设置手机号
     * @param phoneNum
     */
    void setPhoneNum(String phoneNum);
}
