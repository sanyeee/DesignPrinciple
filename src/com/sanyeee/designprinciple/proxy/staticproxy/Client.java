package com.sanyeee.designprinciple.proxy.staticproxy;

public class Client {

    public static void main(String[] args)
    {
        IGamePlayer gamePlayer = new GamePlayer();
        IGamePlayer proxy = new Proxy(gamePlayer);

        proxy.login("lxw", "12346");
        proxy.killBoss();
        proxy.upgrade();
    }

}
