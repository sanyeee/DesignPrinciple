package com.sanyeee.designprinciple.proxy.staticproxy;

public class GamePlayer implements IGamePlayer {

    @Override
    public void login(String name, String password) {
        System.out.println(name + "用户已登录");
    }

    @Override
    public void upgrade() {
        System.out.println("用户又升了一级");
    }

    @Override
    public void killBoss() {
        System.out.println("杀死boss");
    }
}
