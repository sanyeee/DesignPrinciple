package com.sanyeee.designprinciple.proxy.staticproxy;

public class Proxy implements IGamePlayer {

    private IGamePlayer gamePlayer;

    public Proxy(IGamePlayer gamePlayer)
    {
        this.gamePlayer = gamePlayer;
    }

    @Override
    public void login(String name, String password) {
        if (gamePlayer == null)
        {
            System.out.println("用户不存在");
        }

        this.gamePlayer.login(name, password);
    }

    @Override
    public void upgrade() {
        gamePlayer.upgrade();
    }

    @Override
    public void killBoss() {
        gamePlayer.killBoss();
    }
}
