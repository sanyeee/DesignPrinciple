package com.sanyeee.designprinciple.proxy.staticproxy;

public interface IGamePlayer {

    void login(String name, String password);

    void upgrade();

    void killBoss();

}
